export const state = () => ({
    email: '',
    password: '',
    username: '',
    accessToken: '',
    id: '',
})

export const mutations = {
    saveInfo(state, data) {
        state.email = data.email
        state.password = data.password
        state.username = data.username
        state.accessToken = data.accessToken
        state.id = data.id
    },
    clearInfo() {
        state.email = ''
        state.password = ''
        state.username = ''
        state.accessToken = ''
        state.id = ''
    }
}